from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import search

# Create your tests here.
class UnitTest(TestCase):
    def setUp(self):
        self.client = Client()
    
    def test_ada_url(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)
    
    def test_views_search_terpakai(self):
        response = resolve(reverse('books:books'))
        self.assertEqual(response.func, search)

    def test_memakai_base_html(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'base.html')

    def test_ada_tabel_di_html(self):
        response = self.client.get('')
        content = response.content.decode('utf8')
        self.assertIn('<table ', content)

    def test_ada_tombol_search_di_html(self):
        response = self.client.get('')
        content = response.content.decode('utf8')
        self.assertIn('<input type="submit" ', content)

    def test_ada_search_bar_di_html(self):
        response = self.client.get('')
        content = response.content.decode('utf8')
        self.assertIn('<input type="text" ', content)